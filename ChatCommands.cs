﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using MarkGwilliam.com.Framework.Convert;
using MySqlConnector;
using System.IO;
using System.Text;
using System.Threading;

namespace Merthsoft.DecBot3 {
	partial class DecBot {
		private static void BaseConvert(string sender, string channel, string command, IList<string> parameters) {
			if (parameters.Count == 0 || parameters.Count > 3) {
				SendMessage(channel, "{0}: Syntax: !base <number> <fromBase> <toBase>", sender);
				return;
			}

			var number = parameters[0];
			var fromBase = number.StartsWith("0b", "0B") ? 2 : number.StartsWith("0x", "0X", "$") || number.EndsWith("H", "h") ? 16 : 10;
			var toBase = number.StartsWith("0x", "0X", "$", "0b", "0B") || number.EndsWith("H", "h") ? 10 : 16;

			if (parameters.Count == 2) {
				//toBase = int.Parse(parameters[1]);
				if (!int.TryParse(parameters[1], out toBase)) {
					SendMessage(channel, "{0}: Could not parse toBase: {1}.", sender, parameters[1]);
					return;
				}
			} else if (parameters.Count == 3) {
				if (!int.TryParse(parameters[1], out fromBase)) {
					SendMessage(channel, "{0}: Could not parse fromBase: {1}.", sender, parameters[1]);
					return;
				}
				if (!int.TryParse(parameters[2], out toBase)) {
					SendMessage(channel, "{0}: Could not parse toBase: {1}.", sender, parameters[2]);
					return;
				}
			}

			//Convert.ToInt32(number.RemoveAll("0x", "0X", "$", "0b", "0B", "H", "h"), fromBase).ToString(
			try {
				var output = Converter.Create(fromBase, toBase).Convert(number.RemoveAll("0x", "0X", "$", "0b", "0B", "H", "h"));
				SendMessage(channel, "{0}: {1}", sender, output);
			} catch (Exception ex) {
				SendMessage(channel, "{0}: Could not parse {1}: {2}.", sender, number, ex.Message);
			}
		}

		private static void ResetScope(string messageSender, string channel, string command, IList<string> parameters) {
			if (!HasPrivs(messageSender, channel)) {
				return;
			}
			ResetScope();
			SendMessage(channel, "Scope reset.");
		}

		private static void GetTotals(string messageSender, string channel, string command, IList<string> parameters) {
			using (Connection.Open())
			using (Command = new MySqlCommand(SharedSqlCommands.CountTotals, Connection)) {
				using (var reader = Command.ExecuteReader(CommandBehavior.SingleResult)) {
					if (reader.Read()) {
						SendMessage(channel, 
							"The total score is {0} points between {1} names, with {2} links. There are a total of {3} quotes added by {4} people.", 
							reader["TotalScore"], reader["NameCount"], reader["LinkCount"], reader["TotalQuotes"], reader["AddedByCount"]
						);
					} else {
						SendMessage(channel, "Unable to get totals.");
					}
				}
			}
		}
	}
}
