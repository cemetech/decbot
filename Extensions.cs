﻿using MySqlConnector;

namespace Merthsoft.DecBot3
{
    static class Extensions {
		public static MySqlDataReader ExecuteFullComand(this MySqlCommand c, string CommandText, params dynamic[] parameters) {
			c.CommandText = CommandText;
			c.Parameters.Clear();
			foreach (var d in parameters) {
				c.Parameters.AddWithValue(d.Name, d.Value);
			}

			return c.ExecuteReader();
		}

		public static int ExecuteFullUpdate(this MySqlCommand c, string CommandText, params dynamic[] parameters) {
			c.CommandText = CommandText;
			c.Parameters.Clear();
			foreach (var d in parameters) {
				c.Parameters.AddWithValue(d.Name, d.Value);
			}

			return c.ExecuteNonQuery();
		}

		public static bool StartsWith(this string s, params string[] args) {
			foreach (var a in args) {
				if (s.StartsWith(a)) {
					return true;
				}
			}

			return false;
		}

		public static bool EndsWith(this string s, params string[] args) {
			foreach (var a in args) {
				if (s.EndsWith(a)) {
					return true;
				}
			}

			return false;
		}

		public static string RemoveAll(this string s, params string[] args) {
			var ret = s;
			foreach (var a in args) {
				ret = ret.Replace(a, "");
			}

			return ret;
		}
	}

}
