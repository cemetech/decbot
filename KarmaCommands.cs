﻿using MySqlConnector;
using System.Collections.Generic;
using System.Data;
using System.Threading;

namespace Merthsoft.DecBot3
{
    partial class DecBot {
		private static string getKarma(string link, out int score) {
			using (var reader = Command.ExecuteFullComand(KarmaSqlCommands.GetScoreWithLink, new { Name = "@name", Value = link })) {
				if (reader.Read()) {
					score = reader.GetInt32("score");
					return reader.GetString("name");
				} else {
					score = 0;
					return null;
				}
			}
		}

		private static void GetKerma(string messageSender, string channel, string command, IList<string> parameters) {
			GetKarma(messageSender, channel, command, new[] { "KermM" });
		}

		private static void GetKarma(string messageSender, string channel, string command, IList<string> parameters) {
			if (parameters.Count > 1) {
				SendMessage(channel, "Syntax: !karma <name>.");
				return;
			}

			string checkedName;
			if (parameters.Count == 1) {
				checkedName = parameters[0].ToLowerInvariant();
			} else {
				checkedName = messageSender.ToLowerInvariant();
			}

			using (Connection.Open())
			using (Command = new MySqlCommand() { Connection = Connection }) {
				int score;
				var name = getKarma(checkedName, out score);
				if (name == checkedName) {
					SendMessage(channel, "{0} has a score of {1}.", name, score);
				} else if (name != null) {
					SendMessage(channel, "{0} ({2}) has a score of {1}.", name, score, checkedName);
				} else {
					SendMessage(channel, "{0} has a score of 0.", checkedName);
				}
			}		
		}

		private static void GetTop(string messageSender, string channel, string command, IList<string> parameters) {
			using (Connection.Open())
			using (Command = new MySqlCommand(KarmaSqlCommands.GetTop, Connection))
			using (var reader = Command.ExecuteReader(CommandBehavior.Default)) {
				SendMessage(channel, "The top scores are:");
				while (reader.Read()) {
					SendMessage(channel, "{0}: {1}", reader.GetString("Name"), reader.GetInt32("Score"));
					Thread.Sleep(lineTimeout);
				}
			}
		}

		private static void SaySite(string messageSender, string channel, string command, IList<string> parameters) {
			SendMessage(channel, "{0}: {1}", messageSender, site);
		}
	}
}
