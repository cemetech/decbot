﻿using MySqlConnector;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Merthsoft.DecBot3
{
    partial class DecBot {
		private static string getLinkedName(string name) {
			using (var reader = Command.ExecuteFullComand(LinkSqlCommands.GetLink, new { Name = "@name", Value = name })) {
				if (reader.Read()) {
					return reader.GetString("Link");
				} else {
					return null;
				}
			}
		}

		private static void LinkNames(string messageSender, string channel, string command, IList<string> parameters) {
			if (parameters.Count == 1) {
				CheckLinks(messageSender, channel, command, parameters);
				return;
			}

			if (!HasPrivs(messageSender, channel)) {
				return;
			}

			if (parameters.Count != 2) {
				SendMessage(channel, "Syntax: !link <link> <main name>.");
				return;
			}
			var main = parameters[1].ToLowerInvariant();
			var link = parameters[0].ToLowerInvariant();

			if (main == link) {
				SendMessage(channel, "Can't like name to itself.");
				return;
			}

			var fname = main;

			using (Connection.Open())
			using (Command = new MySqlCommand() { Connection = Connection }) {
				// Get final name, and check for a circular link.
				fname = getLinkedName(main) ?? main;
				if (fname == link) {
					SendMessage(channel, "Trying to create a circular link. Nice try. ;)");
					return;
				}
				// Check to see if link exists.
				var linkedName = getLinkedName(link);
				if (linkedName != null) {
					SendMessage(channel, "Link {0} already exists as {1}.", link, linkedName);
					return;
				}
				// Get the old name's score for tracking
				var oldScore = 0;
				getKarma(link, out oldScore);
				// Insert the link into the table.
				var rows = Command.ExecuteFullUpdate(LinkSqlCommands.InsertLink, new { Name = "@link", Value = link }, new { Name = "@fname", Value = fname });
				if (rows == 0) {
					SendMessage(channel, "Failed to add link.");
					return;
				}
				if (fname == main) {
					SendMessage(channel, "Link {0} => {1} added.", link, main);
				} else {
					SendMessage(channel, "Link {0} => {1} => {2} added.", link, main, fname);
				}
				// Update associated links
				rows = Command.ExecuteFullUpdate(LinkSqlCommands.UpdateLinks, new { Name = "@fname", Value = fname }, new { Name = "@link", Value = link });
				if (rows > 0) {
					SendMessage(channel, "Had to update {0} links.", rows);
				}
				// Check if there's a score in the DB, and update if necessary.
				UpdateScores(channel, link, fname, oldScore);
				// Check if there are quotes in the DB, and update if necessary.
				UpdateQuotes(channel, link, fname);
			}
		}

		private static void UpdateQuotes(string channel, string link, string fname) {
			var rows = Command.ExecuteFullUpdate(QuoteSqlCommands.UpdateQuoteLink, new { Name = "@fname", Value = fname }, new { Name = "@name", Value = link });
			if (rows > 0) {
				SendMessage(channel, "Had to update {0} quotes.", rows);
			}
		}

		private static void UpdateScores(string channel, string link, string fname, int oldScore) {
			using (var sumReader = Command.ExecuteFullComand(KarmaSqlCommands.GetSum, new { Name = "@link", Value = link }, new { Name = "@fname", Value = fname })) {
				if (sumReader.Read()) {
					try {
						var total = sumReader.GetInt32("Total");
						sumReader.Close();
						// Perform the update
						var rows = Command.ExecuteFullUpdate(KarmaSqlCommands.InsertScore, new { Name = "@fname", Value = fname }, new { Name = "@score", Value = total });
						if (rows == 0) {
							SendMessage(channel, "Could not update linked scores...");
							return;
						}
						// Track the score change
						rows = Command.ExecuteFullUpdate(KarmaSqlCommands.TrackScoreChange, new { Name = "@name", Value = fname }, new { Name = "@change", Value = oldScore });
						if (rows == 0) {
							SendMessage(channel, "Could not add tracking row for {0}.", fname);
						}
						// Delete the link's score
						rows = Command.ExecuteFullUpdate(KarmaSqlCommands.DeleteScore, new { Name = "@link", Value = link });
						if (rows == 0) {
							SendMessage(channel, "Could not remove link's score...");
							return;
						}
						rows = Command.ExecuteFullUpdate(KarmaSqlCommands.TrackScoreChange, new { Name = "@name", Value = link }, new { Name = "@change", Value = -oldScore });
						if (rows == 0) {
							SendMessage(channel, "Could not add tracking row for {0}.", link);
						}
					} catch {
						SendMessage(channel, "Adding link failed.");
						return;
					}
				}
			}
			return;
		}

		private static void UnlinkNames(string messageSender, string channel, string command, IList<string> parameters) {
			if (!HasPrivs(messageSender, channel)) {
				return;
			}

			if (parameters.Count != 1) {
				SendMessage(channel, "Syntax: !unlink <name>.", messageSender);
				return;
			}

			var name = parameters[0].ToLowerInvariant();

			using (Connection.Open())
			using (Command = new MySqlCommand() { Connection = Connection }) {
				// Check to see if link exists.
				var old = getLinkedName(name);
				if (old == null) {
					SendMessage(channel, "Link {0} does not exist.", name);
					return;
				}
				// Remove it
				var rows = Command.ExecuteFullUpdate(LinkSqlCommands.DeleteLink, new { Name = "@name", Value = name });
				if (rows >= 1) {
					SendMessage(channel, "Link: {0} => {1} removed.", name, old);
				} else {
					SendMessage(channel, "Link {0} does not exist.", name);
				}
			}
		}

		private static void CheckLinks(string messageSender, string channel, string command, IList<string> parameters) {
			if (parameters.Count > 1) {
				SendMessage(channel, "Syntax: !links <name>.");
				return;
			}

			string checkedName;
			if (parameters.Count == 1) {
				checkedName = parameters[0].ToLowerInvariant();
			} else {
				checkedName = messageSender.ToLowerInvariant();
			}

			using (Connection.Open())
			using (Command = new MySqlCommand(LinkSqlCommands.GetLinks, Connection)) {
				Command.Parameters.AddWithValue("@name", checkedName);

				using (var reader = Command.ExecuteReader(CommandBehavior.SingleResult)) {
					if (reader.HasRows) {
						var links = new StringBuilder();
						string name = null;
						while (reader.Read()) {
							name = reader["Link"].ToString();
							var link = reader["Name"].ToString();
							links.AppendFormat("{0}, ", link);
						}
						links.Length -= 2;
						if (name == checkedName) {
							SendMessage(channel, "{0} is linked to: {1}.", name, links);
						} else {
							SendMessage(channel, "{0} ({2}) is linked to: {1}.", name, links, checkedName);
						}
					} else {
						SendMessage(channel, "{0} has no links.", checkedName);
					}
				}
			}
		}
	}
}
