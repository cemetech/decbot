﻿using CommandLine;
using Meebey.SmartIrc4net;
using Merthsoft.DynamicConfig;
using Microsoft.CSharp.RuntimeBinder;
using MySqlConnector;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace Merthsoft.DecBot3
{
    partial class DecBot {
		private const string PlusPlusRegEx = "\\S+(?=\\+\\+)";

		private static bool QuitFlag = false;

		private static IrcClient irc;

		private static DisposableConnection Connection { get; set; }
		private static MySqlCommand Command { get; set; }

		private static Dictionary<string, Action<string, string, string, string[]>> ChatCommandProcessors;
		private static Dictionary<string, Action<string[]>> ConsoleCommandProcessors;

		private static Random random;

		private static TextWriter logWriter;
		private static string configLocation;

		private static Dictionary<string, Regex> chatBots;
		private static int randomChance;
		private static List<string> randomResponses;
		private static int lineTimeout;
		private static string site;
        private static HashSet<string> autoOpHosts;

        private static dynamic config;

		public class CommandLineOptions
		{
			[Option(shortName: 'l', longName: "logfile", Required = false, HelpText = "Name of a file to log to, or stderr by default", Default = null)]
			public string LogPath { get; set; }

			[Value(index: 0, Default = "decbot.ini", HelpText = "Path to the configuration file to use")]
			public string ConfigFile { get; set; }
		}

		static void Main(string[] args) {
			Parser.Default.ParseArguments<CommandLineOptions>(args)
				.WithParsed(Run)
				.WithNotParsed(e => {
					foreach (var err in e) {
						Console.Error.WriteLine(err.ToString());
					}
				});
		}

		static void Quit()
			=> QuitFlag = true;

		static void Run(CommandLineOptions options) {
			if (options.LogPath != null) {
				logWriter = new StreamWriter(options.LogPath, true, Encoding.UTF8);
			} else {
				logWriter = Console.Error;
			}
			configLocation = options.ConfigFile;

			WriteConsoleMessage("Starting DecBot.", MessageDirection.Notification);
			ResetScope();
			InitializeCommandProcessors();
			config = ReloadConfig();
			
			using var t = Task.Run(ReadConsoleCommands);
            connect();

            WriteConsoleMessage("Shutting down.", MessageDirection.Notification);
        }

		private static void connect() {
			WriteConsoleMessage(string.Format("Connecting to: {0} ", config.IRC.server), MessageDirection.Notification);
			irc = new IrcClient() {
				SendDelay = 400,
				ActiveChannelSyncing = true,
				CtcpVersion = config.IRC.realname, 
				AutoReconnect = true,
				AutoRejoin = true,
				AutoRejoinOnKick = true,
				AutoRelogin = true
			};

			irc.OnChannelMessage += OnChannelMessageReceived;
			irc.OnRawMessage += OnRawMessage;
			irc.OnWriteLine += OnWriteLine;
			irc.OnDisconnected += OnDisconnected;
            irc.OnJoin += OnJoin;
            irc.OnOp += OnOp;
            irc.OnDeop += OnDeOp;

            // the server we want to connect to, could be also a simple string
            var serverList = new string[] { config.IRC.server };
			var port = 6667;

			try {
				irc.Connect(serverList, port);
			} catch (ConnectionException e) {
                Console.WriteLine("Couldn't connect! Reason: " + e.Message);
				//Exit();
			}

			irc.Login(config.IRC.nickname, config.IRC.realname, 0, config.IRC.nickname);

			foreach (string chan in config.IRC.channels.Split(',')) {
				irc.RfcJoin(chan);
			}

			while (!QuitFlag)
			{
				try
				{
					irc.Listen(false);
				} catch (Exception ex)
				{
					WriteExceptionToConsole("Exception listening to irc.", ex);
				}
				Thread.Sleep(1);
			}
			try
			{
				irc.RfcQuit();
				irc.Disconnect();
			}
			catch { }
		}

		private static dynamic ReloadConfig() {
			var config = Config.ReadIni(configLocation);

			chatBots = new Dictionary<string, Regex>();
			try
			{
				if (config?.ChatBots != null)
				{
					foreach (var bot in config?.ChatBots)
					{
						try
						{
							chatBots[bot.Key.ToLower()] = new Regex(bot.Value, RegexOptions.IgnoreCase);
						}
						catch (Exception ex)
						{
							WriteErrorToConsole("Cannot load chatbot {0}, error in regex {1}: {2}", bot.Key, bot.Value, ex.Message);
							chatBots[bot.Key.ToLower()] = null;
						}
					}
				}
			} catch (RuntimeBinderException) {
				WriteConsoleMessage("No chat bots configured.", MessageDirection.Notification);
			} catch (Exception ex) {
				WriteExceptionToConsole("Exception reading chat bots.", ex);
			}

            autoOpHosts = new HashSet<string>();

			try
			{
				if (config?.AutoOp != null)
				{
					foreach (var kvp in config?.AutoOp)
					{
						try
						{
							var host = (kvp.Value as string)!;
							var hostParts = host.Split('~');
							if (hostParts.Length > 1)
								host = hostParts[1];
							autoOpHosts.Add(host.ToUpper());
						}
						catch (Exception ex)
						{
							WriteExceptionToConsole($"Couldn't handle autoop {kvp}.", ex);
						}
					}
				}
			}
			catch (RuntimeBinderException)
			{
				WriteConsoleMessage("No Auto op hosts configured.", MessageDirection.Notification);
			}
			catch (Exception ex)
			{
				WriteExceptionToConsole("Exception reading auto op hosts.", ex);
			}

            randomResponses = new List<string>(config.Bot.randomResponses.Split(','));
			randomChance = int.Parse(config.Bot.randomChance);
			lineTimeout = int.Parse(config.Bot.lineTimeout);
			site = config.Bot.site;

			Console.Title = config.IRC.realname;

			string connectionString;
			try {
				connectionString = config.Database.connectionstring;
			} catch {
				var protocol = "socket";
				try {
					protocol = config.Database.protocol;
				} catch {
					WriteErrorToConsole("Protocol not defined, defaulting to socket.");
				}
				connectionString = string.Format("Server={0};Database={2};Uid={3};Pwd={1};Protocol={4}", config.Database.server, config.Database.password, config.Database.database, config.Database.user, protocol);
			}
			Connection = new DisposableConnection(new MySqlConnection(connectionString));

			return config;
		}

		static bool shouldOp(string ident, string host)
			=> autoOpHosts.Contains($"{ident.Trim('~')}@{host}".ToUpper());

        static void OnDeOp(object sender, DeopEventArgs e)
		{
			try
			{
				var user = irc.GetChannelUser(e.Channel, e.Whom);
				if (user != null && shouldOp(user.Ident, user.Host))
					irc.Op(e.Channel, e.Whom);

				AutoOpChannel(e.Channel);
			} catch (Exception ex)
			{
                WriteExceptionToConsole("Exception in OnDeOp", ex);
            }
		}

        static void OnOp(object sender, OpEventArgs e)
        {
            if (e.Whom != irc.Nickname)
                return;

            try
            {
                AutoOpChannel(e.Channel);
            }
            catch (Exception ex)
            {
                WriteExceptionToConsole("Exception in OnOp", ex);
            }
        }

        private static void AutoOpChannel(string channelName)
        {
            var channel = irc.GetChannel(channelName);
            foreach (var entry in channel.Users.Cast<DictionaryEntry>())
            {
                var user = entry.Value as ChannelUser;
                try
                {
                    if (shouldOp(user.Ident, user.Host) && !user.IsOp)
                        irc.Op(channel.Name, user.Nick);
                }
                catch (Exception ex)
                {
                    WriteExceptionToConsole($"Exception in OnOp trying to op {user.Nick} {user.Ident} {user.Host}", ex);
                }
            }
        }

        static void DoAutoOp()
		{
			if (autoOpHosts.Count == 0 || irc.JoinedChannels.Count == 0)
				return;

			foreach (var channel in irc.JoinedChannels)
			{
				try
				{
					AutoOpChannel(channel);
				}
                catch (Exception ex)
                {
                    WriteExceptionToConsole($"Exception in DoAutoOp trying to op in channel {channel}.", ex);
                }
            }
		}

        static void OnJoin(object sender, JoinEventArgs e)
        {
            try
            {
                if (shouldOp(e.Data.Ident, e.Data.Host))
                    irc.Op(e.Data.Channel, e.Data.Nick);
            }
            catch (Exception ex)
            {
                WriteExceptionToConsole("Exception in OnJoin", ex);
            }
        }

        static void OnDisconnected(object sender, EventArgs e)
        {
            try
            {
                LogMessage("server", MessageDirection.Notification, "Disconnected from server. Attempting reconnect.");
                WriteConsoleMessage("Disconnected from server. Attempting reconnect.", MessageDirection.Notification);
                while (!irc.IsConnected)
                {
                    connect();
                    Thread.Sleep(2000);
                }
            }
            catch (Exception ex)
            {
                WriteExceptionToConsole("Exception in OnDisconnected", ex);
            }
        }

        private static void InitializeCommandProcessors() {
			ChatCommandProcessors = new Dictionary<string, Action<string, string, string, string[]>>() {
				// Karma stuff
				{"!karma", GetKarma},
				{"!kamra", GetKarma},
				{"!karm", GetKarma},
				{"!krama", GetKarma},
				{"!top", GetTop},
				{"!link", LinkNames},
				{"!unlink", UnlinkNames},
				{"!links", CheckLinks},
				{"!kerma", GetKerma},
				// Various other things
				{"!resetscope", ResetScope},
				{"!conv", BaseConvert},
				{"!base", BaseConvert},
				{"!total", GetTotals},
				//{"!merthese", Merthterpret},
				// Quotes
				{"!q", GetQuote},
				{"!qsay", GetQuote},
				{"!quote", GetQuote},
				{"!qfind", GetQuote},
				{"!qsearch", GetQuote},
				{"!qadd", AddQuote},
				{"!quoteadd", AddQuote},
				{"!addquote", AddQuote},
				{"!qdel", DeleteQuote},
				{"!site", SaySite},
			};

			ConsoleCommandProcessors = new Dictionary<string, Action<string[]>>() {
				{"/say", SendMessage},
				{"/me", SendAction},
				{"/join", JoinChannels},
				{"/j", JoinChannels},
                {"/part", PartChannels},
                {"/p", PartChannels},
                {"/cycle", CycleChannels},
                {"/c", CycleChannels},
                {"/list", ListChannels},
				{"/ls", ListChannels},
				{"/l", ListChannels},
				{"/listchannels", ListChannels},
				{"/sayall", SendMessageToAll},
				{"/reload", ReloadConfig},
				{"/help", PrintHelp},
				{"/nick", ChangeName},
				{"/op", OpUser},
                {"/quit", Quit},
                {"/q", Quit},
            };
		}

        public static void ReadConsoleCommands()
        {
            while (!QuitFlag)
            {
                try
                {
                    string message = Console.ReadLine();
                    string command;
                    string[] args;
                    string[] msg = message.TrimEnd(' ').Split(' ');

                    command = msg[0];
                    args = msg.Skip(1).ToArray();

					try
					{
						if (ConsoleCommandProcessors.TryGetValue(command, out var value))
						{
							value(args);

						}
						else
						{
							irc?.WriteLine(message);
						}
					}
					catch (Exception ex)
					{
						WriteConsoleMessage(string.Format("Unable to perform command: {0}", ex.Message), MessageDirection.Error);
					}
                }
                catch (Exception ex)
                {
                    WriteExceptionToConsole("Exception in ReadCommands", ex);
                }

                Thread.Sleep(1);
            }
        }

        private static void OnChannelMessageReceived(object sender, IrcEventArgs e) 
		{
			try
			{
				var messageSender = e.Data.Nick;
				var message = e.Data.Message;
				string command;
				string[] args;
				string[] msg;

				if (chatBots.ContainsKey(messageSender.ToLower()))
				{
					var regex = chatBots[messageSender.ToLower()];
					if (regex == null) { return; } // Ignore chatbots whose regex didn't load right
					var matches = regex.Match(message);
					if (!matches.Success) { return; } // Skip if there's no match.
					messageSender = matches.Groups["sender"].Value;
					message = matches.Groups["message"].Value;
				}

				msg = message.TrimEnd(' ').Split(' ');
				command = msg[0];
				args = msg.Skip(1).ToArray();

				if (ReadChatCommand(messageSender, e, command, args))
				{
					return;
				}

				if (random.Next(randomChance) == 0)
				{
					var choice = random.Next(randomResponses.Count);
					var responses = randomResponses[choice].Split('\\');
					foreach (var response in responses)
					{
						if (response.StartsWith("/me "))
						{
							SendAction(e.Data.Channel, response.Substring(4));
						}
						else
						{
							SendMessage(e.Data.Channel, response);
						}
						Thread.Sleep(lineTimeout);
					}
				}

				if (Regex.IsMatch(message, PlusPlusRegEx))
				{
					LogMessage(e.Data.Channel, MessageDirection.In, "<{0}> {1}", e.Data.Nick, e.Data.Message);
					HandlePlusPlus(e.Data.Channel, messageSender, message);
				}
			}
			catch (Exception ex)
			{
				WriteExceptionToConsole("Exception in OnChannelMessageReceived", ex);
			}
        }

		private static void HandlePlusPlus(string channel, string messageSender, string message) {
			foreach (var matchGroup in Regex.Matches(message, PlusPlusRegEx).Cast<Match>().GroupBy(m => m.Value)) {
				var match = matchGroup.First();
				var change = matchGroup.Count();
				var name = match.Value.ToLowerInvariant();
				if (name == "") { continue; }

				using (Connection.Open())
				using (Command = new MySqlCommand() { Connection = Connection }) {
					// Get the final name
					name = getLinkedName(name) ?? name;
					// See if we're the same person
					messageSender = getLinkedName(name) ?? messageSender;
					// Punish cheaters by decrementing
					change *= messageSender.Equals(name, StringComparison.InvariantCultureIgnoreCase) ? -1 : 1;
					int score;
					// Get the existing score
					getKarma(name, out score);
					score += change;
					// Update the score
					var rows = Command.ExecuteFullUpdate(KarmaSqlCommands.InsertScore, new { Name = "@fname", Value = name }, new { Name = "@score", Value = score });
					if (rows == 0) {
						SendMessage(channel, "Could not update score for {0}.", name);
						return;
					}
					// Track the score change
					rows = Command.ExecuteFullUpdate(KarmaSqlCommands.TrackScoreChange, new { Name = "@name", Value = name }, new { Name = "@change", Value = change });
					if (rows == 0) {
						SendMessage(channel, "Could not add tracking row for {0}.", name);
					}
				}
			}
		}

        private static void WriteExceptionToConsole(string text, Exception ex)
        {
            WriteConsoleMessage(ex.ToString(), MessageDirection.Error);
            WriteConsoleMessage(text, MessageDirection.Error);
        }

        private static void WriteErrorToConsole(string format, params object[] args) {
			WriteConsoleMessage(string.Format(format, args), MessageDirection.Error);
		}

		private static void SendMessage(string channel, string format, params object[] args) {
			irc.SendMessage(SendType.Message, channel, string.Format(format, args));
			LogMessage(channel, MessageDirection.Out, format, args);
		}

		private static void SendAction(string channel, string format, params object[] args) {
			irc.SendMessage(SendType.Action, channel, string.Format(format, args));
			LogMessage(channel, MessageDirection.Out, "/me " + format, args);
		}

		private static string getDirectionString(MessageDirection direction) {
			ConsoleColor color;
			return getDirectionString(direction, out color);
		}

		private static string getDirectionString(MessageDirection direction, out ConsoleColor color) {
			var dirString = "";
			switch (direction) {
				case MessageDirection.In:
					color = ConsoleColor.Green;
					dirString = ">>>";
					break;
				case MessageDirection.Out:
					color = ConsoleColor.Cyan;
					dirString = "<<<";
					break;
				default:
				case MessageDirection.Notification:
					color = ConsoleColor.Yellow;
					dirString = "---";
					break;
				case MessageDirection.Error:
					color = ConsoleColor.Red;
					dirString = "!!!";
					break;
			}
			return dirString;
		}

		private static void LogMessage(string channel, MessageDirection direction, string format, params object[] args) {
			var dir = getDirectionString(direction);

			logWriter.WriteLine("{0} [{1}] {2} {3}", dir,
				DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss.fff"),
				channel, string.Format(format, args)
			);
			logWriter.Flush();
		}

		private static bool ReadChatCommand(string messageSender, IrcEventArgs e, string command, string[] args) {
			if (ChatCommandProcessors.ContainsKey(command)) {
				LogMessage(e.Data.Channel, MessageDirection.In, "<{0}> {1}", e.Data.Nick, e.Data.Message);
				try {
					ChatCommandProcessors[command](messageSender, e.Data.Channel, command, args);
				} catch (Exception ex) {
					SendMessage(e.Data.Channel, "{1}: Unable to perform \"{2}\": {0}", ex.Message, e.Data.Nick, command);
					LogMessage(e.Data.Channel, MessageDirection.Error, "ERROR: {0}", ex.ToString());
				}
				return true;
			}
			return false;
		}

		private static void ResetScope() {
			random = new Random();
		}

		private static bool HasPrivs(string userName, Channel channel) {
			foreach (ChannelUser user in channel.Users.Values) {
				if (user.Nick == userName) {
					if (chatBots.ContainsKey(user.Nick)) {
						return false;
					}
					return (user.IsOp || user.IsVoice);
				}
			}

			return false;
		}

		private static bool HasPrivs(string userName, string channelName) {
			return HasPrivs(userName, irc.GetChannel(channelName));
		}

        private static void OnRawMessage(object sender, IrcEventArgs e)
        {
            try
            {
                WriteConsoleMessage(e.Data.RawMessage, MessageDirection.In);
            }
            catch (Exception ex)
            {
                WriteExceptionToConsole("Exception in OnRawMessage", ex);
            }
        }

        private static void OnWriteLine(object sender, WriteLineEventArgs e)
        {
            try
            {
                WriteConsoleMessage(e.Line, MessageDirection.Out);
            }
            catch (Exception ex)
            {
                WriteExceptionToConsole("Exception in OnWriteLine", ex);
            }
        }

        private static void WriteConsoleMessage(string message, MessageDirection direction) {
			var color = ConsoleColor.White;
			var dirString = getDirectionString(direction, out color);

			Console.ForegroundColor = color;
			Console.WriteLine("[{0}] {1} {2}", DateTime.Now.ToString("HH:mm:ss"), dirString, message);
			Console.ForegroundColor = ConsoleColor.White;
		}
	}
}
